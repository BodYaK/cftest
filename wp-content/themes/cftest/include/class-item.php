<?php

class ShopItem{

    private $id;
    public $name;
    public $attributes;
    public $price;
    public $image;
    public $images;
    
    public function save(){
        global $wpdb;

        $sql = "INSERT INTO cf_items (name, image, price) VALUES
        ('$this->name', '$this->image', '$this->price')";
        $wpdb->query($sql);

        $this->id = $wpdb->insert_id;


        if(!empty($this->attributes)){
            $sql = "INSERT INTO cf_item_attributes (itemID, name, value) VALUES ";
            foreach ($this->attributes as $attr){
                $tempQuery = "($this->id,'".$attr["name"]."','".$attr["value"]."'),";
                $sql.=$tempQuery;
            }
            $sql = substr($sql,0,-1);
            $wpdb->query($sql);
        }

        if(!empty($this->images)){
            $sql = "INSERT INTO cf_item_images (itemID, url) VALUES ";
            foreach ($this->images as $img){
                $tempQuery = "($this->id,'".$img["url"]."'),";
                $sql.=$tempQuery;
            }
            $sql = substr($sql,0,-1);
            $wpdb->query($sql);
        }

    }

    public function getId(){
        return $this->id;
    }

    static public function getById($id){
        global $wpdb;

        $obj = new ShopItem();

        $sql = "SELECT * from cf_items where id=$id";

        $array = $wpdb->get_results($sql,ARRAY_A);
        $obj->id = $array[0]["id"];
        $obj->name = $array[0]["name"];
        $obj->price = $array[0]["price"];
        $obj->image = $array[0]["image"];

        $sql = "SELECT * from cf_item_attributes where itemId=$id";

        $obj->attributes = $wpdb->get_results($sql,ARRAY_A );

        $sql = "SELECT * from cf_item_images where itemId=$id";

        $obj->images = $wpdb->get_results($sql,ARRAY_A );
        
        return $obj;
    }
}