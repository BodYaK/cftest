<?php
require('include/simple_html_dom.php');
require('include/class-item.php');

function parseEbay($url){
    $html = file_get_html($url);
    $item = new ShopItem();

    $elem = $html->find('#itemTitle');
    $item->name = preg_replace("'<span[^>]*?>.*?</span>'si","",$elem[0]->innertext);

    $elem = $html->find('#prcIsum');
    $item->price = $elem[0]->innertext;

    $elem = $html->find('#icImg');
    $item->image = $elem[0]->src;

    $elem = $html->find('div.itemAttr table[!id]');
    $elem = $elem[0]->find('td');
    $attrArray = array();
    for($i=0;$i<count($elem);$i=$i+2){
        if($elem[$i]->class == "attrLabels"){
            $nameTemp = str_replace(":","",$elem[$i]->plaintext);
            $tempArray["name"] = $nameTemp;
            $tempArray["value"] = $elem[$i+1]->plaintext;
            $attrArray[] = $tempArray;
        }
    }
    $item->attributes = $attrArray;

    $elem = $html->find('div#vi_main_img_fs img');
    $imgArray = array();
    foreach ($elem as $e){
        $imgArray[]["url"] = str_replace("s-l64","s-l500",$e->src);
    }
    $item->images = $imgArray;
    
    $item->save();

    return $item->getId();
}