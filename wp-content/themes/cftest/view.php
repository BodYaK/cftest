<?php
/*
 Template Name: view
 */
if(isset($_GET["id"])):
    $obj = ShopItem::getById($_GET["id"]);
?>

    <h2><?=$obj->name?></h2>
    <img src="<?=$obj->image?>">
    <div>price: <?=$obj->price?></div>
    <div style="width: 100%">
        <div style="width: 50%; display: inline-block">
            <?php foreach ($obj->images as $img):?>
            <div style="width: 33%; display: inline-block">
                <img width="100%" src="<?=$img["url"]?>">
            </div>
            <?php endforeach;?>
        </div>
        <div style="width: 50%; display: inline-block">
            <?php foreach ($obj->attributes as $attr):?>
                <div>
                    <?= $attr["name"]?>:<?=$attr["value"]?>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    
<?php endif;

